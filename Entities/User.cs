using System.ComponentModel.DataAnnotations;

namespace Absenzio.Model.Entities
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        public string Mail { get; set; }
        public string ForwardTo { get; set; }

        public int ReminderPlanId { get; set; }
        public virtual ReminderPlan ReminderPlan { get; set; }        
    }
}