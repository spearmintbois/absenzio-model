namespace Absenzio.Model.Entities
{
    public class ReminderPlan
    {
        public int Id { get; set; }
        public string Name { get; set; }
        /* Javascript function to caluclate next reminder day */
        public string Equation { get; set; }
    }
}