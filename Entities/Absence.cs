using System;
using System.ComponentModel.DataAnnotations;

namespace Absenzio.Model.Entities
{
    public class Absence
    {
        public int Id { get; set; }
        /* Has this Absence been excused? */
        [Required]
        public bool Excused { get; set; }
        /* When we received the mail for this absence */
        [Required]
        public DateTime ReceivedAt { get; set; }
        /* On what day was he absence */
        [Required]
        public DateTime AbsenceDate { get; set; }
        /* What day should he be reminded next */
        public DateTime RemindNextOn { get; set; }
        /* When did we last remind him */
        public DateTime LastRemindedAt { get; set; }
        /* When does this absence expire, usually AbsenceDate + 4 Weeks */
        [Required]
        public DateTime ExpiresOn { get; set; }
        /* How many times have we reminded him1? */
        [Required]
        public int ReminderCount { get; set; }
    }
}