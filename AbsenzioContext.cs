using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

using Absenzio.Model.Entities;

namespace Absenzio.Model
{
    public class AbsenzioContext : DbContext
    {
        public DbSet<Absence> Absences { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ReminderPlan> ReminderPlans { get; set; }

        public AbsenzioContext(DbContextOptions<AbsenzioContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ReminderPlan>().HasData(new ReminderPlan() {
                Id = 1,
                Name = "Daily",
                Equation = @"function (absence, expires, reminded, count) {
                    var next = new Date();
                    next.setDate(next.getDate() + 1);
                    return next;
                }"
            });
        }
    }
}
